const express = require('express');
const db = require('../sqlfileDb');
const router = express.Router();



router.get('/', async (req, res, next) => {
  try {
    let query = 'SELECT * FROM comment';

    if (req.query.orderBy === 'id') {
      query += ' ORDER BY id WHERE id === news_id';

    }
    let [comment] = await db.getConnection().execute(query);

    // work on that further
    return res.send(comment)

  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.news_id) {
      return res.status(400).send({message: 'This news does not exist'});
    }

    const item = {
      news_id: req.body.news_id,
      description: req.body.description,
    };


    let query = 'INSERT INTO comment (news_id, description) VALUES (?,?)';

    const [results] = await db.getConnection().execute(query, [
      item.news_id,
      item.description,
    ]);

    const id = results.insertId;

    return res.send({message: 'Created new comment', id});
  } catch (e) {
    next(e);
  }

})

router.delete('/:id', async (req, res, next) => {
  try {
    await db.getConnection().execute('DELETE FROM comment WHERE id = ?', [req.params.id]);
    return res.send('Comment is deleted');

  } catch (e) {
    return res.send(`Comment can not be found:(, ${e}`);
  }

})




module.exports = router;