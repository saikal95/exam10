const express = require('express');
const db = require('../sqlfileDb');
const path = require("path");
const multer = require('multer');
const config = require('../config');
const router = express.Router();
const currentDate = new Date();


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, path.extname(file.originalname))
  }
});

const upload = multer({storage});

let categoryAr = [];

router.get('/', async (req, res, next) => {
  try {
    let query = 'SELECT * FROM news';
    let [news] = await db.getConnection().execute(query);

    news.forEach(item =>{
      categoryAr.push({id: item.id,name: item.name, image: item.image, date: item.date})
    })
    return res.send(categoryAr)

  } catch (e) {
    next(e);
  }
});


router.post('/', upload.single('image'), async (req, res, next) => {

  try {

    if (!req.body.name || !req.body.description) {
      return res.status(400).send({message: 'Title and content for the news are required'});
    }

    const item = {
      name: req.body.name,
      description: req.body.description,
      image:null,
      date: currentDate.toISOString()
    };

    if (req.file) {
      item.image = req.file.filename;
    }

    let query = 'INSERT INTO news (name, description,image,date) VALUES (?,?,?,?)';

    const [results] = await db.getConnection().execute(query, [
      item.name,
      item.description,
      item.image,
      item.date
    ]);

    const id = results.insertId;
    return res.send({message: 'Created new item', id});
  } catch (e) {
    next(e);
  }

})


router.get('/:id', async (req, res, next) => {
  try {
    const [news] = await db.getConnection().execute('SELECT * FROM news WHERE id = ?', [req.params.id]);
    const item = news[0];

    if (!item) {
      return res.status(404).send({message: 'Not found'});
    }

    return res.send(item);
  } catch (e) {
    next(e);
  }

})

router.delete('/:id', async (req, res, next) => {

  try {
    await db.getConnection().execute('DELETE FROM news WHERE id = ?', [req.params.id]);
    return res.send('News is deleted');

  } catch (e) {
    return res.send(`News can not be found, ${e}`);
  }

})



module.exports = router;