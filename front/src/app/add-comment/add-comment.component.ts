import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {NewsService} from "../setvice/news.service";

@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.sass']
})
export class AddCommentComponent implements OnInit {
  @ViewChild('f') commentsForm! : NgForm;


  constructor(private newsService: NewsService) { }

  ngOnInit(): void {
  }

  onSubmit() {

  }


}
