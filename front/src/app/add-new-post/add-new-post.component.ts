import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {NewsService} from "../setvice/news.service";
import {NewsData} from "../model/news.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-new-post',
  templateUrl: './add-new-post.component.html',
  styleUrls: ['./add-new-post.component.sass']
})
export class AddNewPostComponent implements OnInit {
  @ViewChild('f')newsForm! : NgForm;


  constructor(private newsService: NewsService, private router: Router) { }

  ngOnInit(): void {}

  onSubmit(){
    const newsData : NewsData  = this.newsForm.value;
    const next = () => {
      this.newsService.getMessages();
      void this.router.navigate(['']);
    };
    this.newsService.postNews(newsData).subscribe(next);

  }


}
