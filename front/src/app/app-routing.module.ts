import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AddNewPostComponent} from "./add-new-post/add-new-post.component";
import {CommentComponent} from "./comment/comment.component";
import {PostsComponent} from "./posts/posts.component";
import {NewsResolverService} from "./news-resolver.service";

const routes: Routes = [
  {path: '', component: PostsComponent},
  {path: 'news/new', component: AddNewPostComponent},
  {path: ':id', component: CommentComponent,
    resolve: {
    news: NewsResolverService
    }

  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
