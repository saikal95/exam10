import {Component, Input, OnInit} from '@angular/core';
import {News} from "../model/news.model";
import {Subscription} from "rxjs";
import {environment} from "../../environments/environment";
import {NewsService} from "../setvice/news.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnInit {

  news! : News;


  constructor( private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.news = <News>data['news'];
    });

  }

}
