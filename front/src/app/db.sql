create table news
(
    id          int auto_increment
        primary key,
    name        varchar(255) not null,
    description text         not null,
    image       varchar(31)  null,
    date        varchar(255) null
);

create table comment
(
    id          int auto_increment
        primary key,
    news_id     int          null,
    author      varchar(255) null,
    description text         not null,
    constraint comment_news_id_fk
        foreign key (news_id) references news (id)
            on delete cascade
);

