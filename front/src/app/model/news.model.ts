export class News {
  constructor(
    public id: number,
    public name: string,
    public description: string,
    public image: string,
    public date: string,
  ) {}
}

export interface NewsData {
  [key: string]: any;
  name: string;
  description: string | null;
  image: File | null;
}

export class Comment {
  constructor(
    public id: number,
    public name: string,
    public description: string,
  ) {}
}

export interface commentData {
   id: number,
  author: string | null;
  description: string ;
}
