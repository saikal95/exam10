import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {News} from "./model/news.model";
import {NewsService} from "./setvice/news.service";
import {EMPTY, mergeMap, Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class NewsResolverService implements Resolve<News>{


  constructor(private newsService: NewsService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<News> | Observable<never> {
    const newsId = <number>route.params['id'];

    return this.newsService.getArticle(newsId).pipe(mergeMap(news => {
      if (news) {
        return of(news);
      }

      void this.router.navigate(['/news']);
      return EMPTY;
    }));
  }

}
