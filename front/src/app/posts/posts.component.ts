import {Component, OnDestroy, OnInit} from '@angular/core';
import {News} from "../model/news.model";
import {Subscription} from "rxjs";
import {NewsService} from "../setvice/news.service";
import {environment} from "../../environments/environment";
import {error} from "@angular/compiler/src/util";
import {Router} from "@angular/router";


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit, OnDestroy {

  news!: News[];
  oneNews! : News;
  newsSubscription!: Subscription;
  apiUrl = environment.apiUrl;

  constructor(private newsService: NewsService) { }

  ngOnInit(): void {

    this.newsSubscription = this.newsService.newsChange.subscribe((news: News[])=> {
      this.news = news;
    })
    this.newsService.getMessages();
  }

  onRemove(id: number) {
    this.newsService.removeNews(id).subscribe(() => {
      this.newsService.getMessages();
    });
  }

  ngOnDestroy(){
    this.newsSubscription.unsubscribe();
  }

}


