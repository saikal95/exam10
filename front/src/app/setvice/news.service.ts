import { Injectable } from '@angular/core';
import {map, Subject, tap} from "rxjs";
import {commentData, News, NewsData} from "../model/news.model";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  newsFetching = new Subject<boolean>();
  newsChange = new Subject<News[]>();
  newsRemoving = new Subject<boolean>();
  news: News[] = [];


  constructor(private http: HttpClient) { }

  getMessages() {
    this.newsFetching.next(true);
    return this.http.get<News[]>(environment.apiUrl + '/news')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return result.map(newsData => {
          return new News (newsData.id, newsData.name, newsData.description, newsData.image, newsData.date);
        });
      }))
      .subscribe(news => {
        this.news = news;
        this.newsChange.next(this.news.slice());
        this.newsFetching.next(false);
      });
  }

  postNews(messageData: NewsData) {
    const formData = new FormData();
    Object.keys(messageData).forEach(key => {

      if(messageData[key] !== null){
        formData.append(key, messageData[key]);
      }
    });
    return this.http.post(environment.apiUrl +'/news', formData);
  }


  getArticle(id: number){
    return this.http.get<News>(environment.apiUrl +'/news/'+id).pipe(
      map(result => {
        return new News(result.id, result.name, result.description, result.image, result.date);
      }),
    );
  }

  removeNews(id: number) {
    this.newsRemoving.next(true);
    return this.http.delete(environment.apiUrl +'/news/'+id);
  };


}
